#include "../apue.h"
#include <sys/shm.h>
#include <errno.h>

int main(int argc, char **argv) {

    char *array;
    int shmid;
    int new = 1;

    char pathname[] = "1.1.c";
    //struct stat st;
    //stat(pathname, &st);
    int st = 1505;

    key_t key;
    if ((key = ftok(pathname, 0) ) < 0) {
        err_sys("Can\'t generate key\n");
    }

    if ((shmid = shmget(key, st /*st.st_size*/ * sizeof(char), 0666 | IPC_CREAT | IPC_EXCL)) < 0) {
        if (errno != EEXIST) {
            err_sys("Can\'t create shared memory\n");
        } else {
            if((shmid = shmget(key, st /*st.st_size*/ * sizeof(char), 0)) < 0){
                err_sys("Can\'t find shared memory\n");
            }
            new = 0;
        }

        if ((array = (char *) shmat(shmid, NULL, 0)) == (char *)(-1)) {
            err_sys("Can't attach shared memory\n");
        }
    }

    int fd;
    size_t len = 1;
    char *str = (char *) malloc(sizeof(char) * len + 1);

    if (new) {
        if ((fd = open(pathname, O_RDONLY, 0666)) < 0) {
            err_sys("Can`t open file\n");
        }
        for (int i = 0; i < st /*st.st_size*/; i++) {
            if (write(fd, str, len) == len)
                array[0] = str[0];
            else
                exit(1);
        }
    } else {
        if ((fd = open(pathname, O_RDONLY, 0666)) < 0) {
            err_sys("Can`t open file\n");
        }
        for (int i = 0; i < st /*st.st_size*/; i++)
            if (read(fd, str, len) == 1)
                printf("%c", str[0]);
            else
                exit(1);
    }

    if (shmdt(array) < 0) {
        err_sys("Can't detach shared memory\n");
    }
    free(str);

    return 0;
}
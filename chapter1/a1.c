#include "../apue.h"

int main(int argc, char **argv, char **envp) {

    printf("User    ID = %d\n", getuid());
    printf("Group   ID = %d\n", getgid());
    printf("This   PID = %d\n", getpid());
    printf("Parent PID = %d\n", getppid());
    return 0;
}
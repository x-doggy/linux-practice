#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

int main(int argc, char **argv, char **envp) {

    // Вывод аргументов командной строки
    while (--argc > 0)
        printf( (argc > 1) ? "%s " : "%s", *++argv );

    puts("\n");

    // Вывод переменных окружения
    for (; *envp != 0; envp++)
        puts(*envp);

    return 0;
}
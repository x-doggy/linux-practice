#include "../apue.h"
#include <pthread.h>

int a = 0;

void *thread1(void *dummy) {
    pthread_t mythid;
    int b = 0;
    mythid = pthread_self();
    b = a;
    b = b + 1;
    a = b;
    printf("Thread %lu. Calculation result = %d\n", mythid, a);
    return NULL;
}

void *thread2(void *dummy) {
    pthread_t mythid;
    int b = 0;
    mythid = pthread_self();
    b = a;
    b = b + 2;
    a = b;
    printf("Thread %lu. Calculation result = %d\n", mythid, a);
    return NULL;
}

void *thread3(void *dummy) {
    pthread_t mythid;
    int b = 0;
    mythid = pthread_self();
    b = a;
    b = b + 3;
    a = b;
    printf("Thread %lu. Calculation result = %d\n", mythid, a);
    return NULL;
}

int main(int argc, char **argv) {

    pthread_t thid1, thid2, thid3;
    int result;

    if ((result = pthread_create(&thid1, (pthread_attr_t *) NULL, thread1, NULL)) != 0) {
        err_quit("Error on thread create, return value = %d\n", result);
    }
    printf("Thread created, thid 1 = %lu\n", thid1);

    if ((result = pthread_create(&thid2, (pthread_attr_t *) NULL, thread2, NULL)) != 0) {
        err_quit("Error on thread create, return value = %d\n", result);
    }
    printf("Thread created, thid 2 = %lu\n", thid2);

    if ((result = pthread_create(&thid3, (pthread_attr_t *) NULL, thread3, NULL)) != 0) {
        err_quit("Error on thread create, return value = %d\n", result);
    }
    printf("Thread created, thid 3 = %lu\n", thid3);

    pthread_join(thid1, (void **) NULL);
    pthread_join(thid2, (void **) NULL);
    pthread_join(thid3, (void **) NULL);
    
    return 0;
}
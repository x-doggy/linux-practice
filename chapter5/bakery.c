#include "../apue.h"
#include <pthread.h>
#define N 3

int a = 0; // Shared resource

int choosing[N];
int number[N];

void enter_region(int process) {
    choosing[process] = 1;

    int max = number[0];
    for (int i=1; i < N - 1; i++) {
        if (number[i] > max)
            max = number[i];
    }

    number[process] = max + 1;
    choosing[process] = 0;

    for (int j=0; j < N; j++) {
        while (choosing[j]);
        while (number[j] != 0 && (number[j] < number[process] || ((number[j] == number[process]) && j < process)));
    }
}

void leave_region(int process) {
    number[process] = 0;
}


// ----- Threads -----

void *thread1(void *dummy) {
    pthread_t mythid;
    int b = 0;
    mythid = pthread_self();
    enter_region(1);
    b = a;
    b = b + 1;
    a = b;
    leave_region(1);
    printf("Thread %lu. Calculation result = %d\n", mythid, a);
    return NULL;
}

void *thread2(void *dummy) {
    pthread_t mythid;
    int b = 0;
    mythid = pthread_self();
    enter_region(2);
    b = a;
    b = b + 2;
    a = b;
    leave_region(2);
    printf("Thread %lu. Calculation result = %d\n", mythid, a);
    return NULL;
}

void *thread3(void *dummy) {
    pthread_t mythid;
    int b = 0;
    mythid = pthread_self();
    enter_region(3);
    b = a;
    b = b + 3;
    a = b;
    leave_region(3);
    printf("Thread %lu. Calculation result = %d\n", mythid, a);
    return NULL;
}

int main(int argc, char **argv) {

    for (int i=0; i < N; i++) {
        choosing[i] = 0;
    }

    pthread_t thid1, thid2, thid3;
    int result;

    if ((result = pthread_create(&thid1, (pthread_attr_t *) NULL, thread1, NULL)) != 0) {
        err_quit("Error on thread create, return value = %d\n", result);
    }
    printf("Thread created, thid 1 = %lu\n", thid1);

    if ((result = pthread_create(&thid2, (pthread_attr_t *) NULL, thread2, NULL)) != 0) {
        err_quit("Error on thread create, return value = %d\n", result);
    }
    printf("Thread created, thid 2 = %lu\n", thid2);

    if ((result = pthread_create(&thid3, (pthread_attr_t *) NULL, thread3, NULL)) != 0) {
        err_quit("Error on thread create, return value = %d\n", result);
    }
    printf("Thread created, thid 3 = %lu\n", thid3);

    pthread_join(thid1, (void **) NULL);
    pthread_join(thid2, (void **) NULL);
    pthread_join(thid3, (void **) NULL);

    return 0;
}
#include "../../apue.h"

int main(int argc, char **argv) {

    int fd1[2], fd2[2], result;
    ssize_t size;
    char resstring1[20], resstring2[21];

    if (pipe(fd1) < 0) {
        err_sys("Can't create pipe 1!");
    }
    if (pipe(fd2) < 0) {
        err_sys("Can't create pipe 2!");
    }

    /*
     * Процесс-родитель создает два
     * pipe, записывает информацию в первый, и ждет поступления сообщения
     * от процесса-потомка во второй pipe. Процесс-потомок читает сообщение
     * из первого pipe и записывает во второй.
     */

    result = fork();
    if (result == -1) {
        // Fail while creating child process, exit
        err_sys("Can't fork child!");
    }
    else if (result > 0) {
        // Parent process, source
        close(fd1[0]);
        close(fd2[1]);

        size = write(fd1[1], "Hello, world, parent!", 21);
        if (size != 21) {
            err_sys("[Parent]: Can't write whole string!");
        }
        close(fd1[1]);

        size = read(fd2[0], resstring1, 20);
        if (size < 0) {
            err_sys("[Parent]: Can't read string!");
        }
        puts(resstring1);

        close(fd2[0]);
        close(fd1[1]);
        puts("Parent exit!\n");
    } else {
        // Child process, destination
        close(fd1[1]);
        close(fd2[0]);

        size = read(fd1[0], resstring2, 21);
        if (size < 0) {
            err_sys("[Child]: Can't read string!");
        }
        puts(resstring2);

        size = write(fd2[1], "Hello, world, child!", 20);
        if (size != 20) {
            err_sys("[Child]: Can't write whole string!");
        }

        close(fd1[0]);
        close(fd2[1]);
        puts("Child exit!\n");
    }
    return 0;
}
#include "../../apue.h"

int main(int argc, char **argv) {

    int fd;
    ssize_t size;
    const char fifo[] = "shared1.fifo";

    umask(0);
    if ((fd = open(fifo, O_RDONLY)) < 0) {
        err_sys("Can't open FIFO for reading!");
    }

    char c = ' ';
    while ( read(fd, &c, 1) == 1 )
        printf("%c", c);
    puts("");
    
    close(fd);
    return 0;
}
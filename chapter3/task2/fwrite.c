#include "../../apue.h"

int main(int argc, char **argv) {

    int fd;
    ssize_t size;
    const char fifo[] = "shared1.fifo";
    struct stat st;

    umask(0);
    if (stat(fifo, &st)) {
        // If FIFO doesn't exists
        if (mknod(fifo, S_IFIFO | 0666, 0) < 0) {
            err_sys("Can't create FIFO!");
        }
        puts("FIFO created successfully!");
    }

    if ((fd = open(fifo, O_WRONLY)) < 0) {
        err_sys("Can't open FIFO for writing!");
    }

    const size_t msglen = strlen(argv[1]);
    printf("%d", msglen);
    size = write(fd, argv[1], msglen);
    if (size != msglen) {
        err_sys("Can't write whole string into FIFO!");
    }

    close(fd);
    return 0;
}
cmake_minimum_required(VERSION 3.6)
project(tasks_fkn)

set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wall")
set(CHAPTER chapter3_task1)

set(SOURCE_FILES write.c)
add_executable(${CHAPTER}_write ${SOURCE_FILES})

set(SOURCE_FILES read.c)
add_executable(${CHAPTER}_read ${SOURCE_FILES})
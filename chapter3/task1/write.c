#include "../../apue.h"

int main(int argc, char **argv) {

    int fd;
    umask(0);
    ssize_t size;
    const size_t msglen = strlen(argv[1]);

    if ((fd = open("example.txt", O_WRONLY | O_CREAT | O_TRUNC, 0666)) < 0) {
        err_sys("Unable to open file for writing!");
    }

    size = write(fd, argv[1], msglen);
    if (size != msglen) {
        err_sys("Can't write string into file!");
    }

    close(fd);
    return 0;
}

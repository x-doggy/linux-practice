#include "../../apue.h"

int main(int argc, char **argv) {

    int fd;
    if ((fd = open("example.txt", O_RDONLY)) < 0) {
        err_sys("Unable to open file for reading!");
    }

    char c = ' ';
    while ( read(fd, &c, 1) == 1 )
        printf("%c", c);
    puts("");

    close(fd);
    return 0;
}
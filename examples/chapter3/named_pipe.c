#include "../../apue.h"

int main() {
    int fd, result;
    ssize_t size;
    char resstring[14];
    const char name[] = "aaa.fifo";

    umask(0);
    if (mknod(name, S_IFIFO | 0666, 0) < 0) {
        err_sys("Can't create FIFO!");
    }

    if ((result = fork()) < 0) {
        err_sys("Can't fork child!");
    } else if (result > 0) {
        // Parent process
        if ((fd = open(name, O_WRONLY)) < 0) {
            err_sys("Can't open FIFO for writing!");
        }
        size = write(fd, "Hello, world", 14);
        if (size != 14) {
            err_sys("Can't write whole string into FIFO!");
        }
        close(fd);
        printf("%s\n", "Parent exit");
    } else {
        // Child process
        if ((fd = open(name, O_RDONLY)) < 0) {
            err_sys("Can't open FIFO for reading!");
        }
        size = read(fd, resstring, 14);
        if (size < 0) {
            err_sys("Can't read string!");
            _exit(-1);
        }
        printf("%s\n", resstring);
        close(fd);
    }
    return 0;
}
#include "../../apue.h"

int main() {
    int fd[2], result;
    ssize_t size;
    char resstring[14];

    // Try to create pipe
    if (pipe(fd) < 0) {
        // If failed then print it and quit
        err_sys("Can't create pipe!");
    }
    result = fork();
    if (result == -1) {
        // Fail while creating child process, exit
        err_sys("Can't fork child!");
    }
    else if (result > 0) {
        // Parent process, source
        close(fd[0]);
        // Try to write 14 bytes into shared buffer
        size = write(fd[1], "Hello, world!", 14);
        if (size != 14) {
            // If wrote less than 14 bytes then quit
            err_sys("Can't write whole string!");
        }
        close(fd[1]);
        printf("%s\n", "Parent exit!");
    } else {
        // Child process, destination
        close(fd[1]);
        // Try to read shared buffer
        size = read(fd[0], resstring, 14);
        if (size < 0) {
            // Fail to read
            err_sys("Can't read string!");
        }
        // Print read string
        printf("%s\n", resstring);
        close(fd[0]);
    }
    return 0;
}
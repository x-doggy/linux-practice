#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>

int main() {
    int *array = NULL;
    int shmid;
    int new = 1;
    const char pathname[] = "06-la.e";
    key_t key;

    // Generate key
    if ((key = ftok(pathname, 0)) < 0) {
        perror("Can't generate key!\n");
        _exit(-1);
    }

    // Create shared memory for generated key
    if ((shmid = shmget(key, 3 * sizeof(int), 0666 | IPC_CREAT | IPC_EXCL)) < 0) {
        // Shared memory segment exists?
        if (errno != EEXIST) {
            perror("Can't create shared memory!\n");
            _exit(-1);
        } else {
            // Shm exists, then try to get IPC descriptor and flush flag if success
            if ((shmid = shmget(key, 3 * sizeof(int), 0)) < 0) {
                perror("Can't find shared memory!\n");
                _exit(-1);
            }
            new = 0;
        }
        // Try to cast shared mem into address space of current process
        if ((array = (int *)shmat(shmid, NULL, 0)) == (int *)-1) {
            perror("Can't attach shared memory!\n");
            _exit(-1);
        }
        // Do the array
        if (new) {
            array[0] = 1;
            array[1] = 0;
            array[2] = 1;
        } else {
            array[0] += 1;
            array[2] += 1;
        }
        printf("SHM ID = %d\t\tnew = %d\n", shmid, new);
        // Print info, detach and exit
        printf("Program 1 was spawn %d times;\nProgram 2 - %d times;\nTotal - %d times\n", array[0], array[1], array[2]);
        if (shmdt(array) < 0) {
            perror("Can't detach shared memory!\n");
            _exit(-1);
        }
    }

    return 0;
}
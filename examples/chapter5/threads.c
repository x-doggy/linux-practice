#include <pthread.h>
#include <stdio.h>
#include <unistd.h>

int a = 0;

void *thread1(void *dummy) {
    pthread_t mythid;
    int b = 0;
    mythid = pthread_self();
    b = a;
    b = b + 1;
    a = b;
    printf("Thread %lu. Calculation result = %d\n", mythid, a);
    return NULL;
}

int main() {
    pthread_t thid, mythid;
    int result = pthread_create(&thid, (pthread_attr_t *) NULL, thread1, NULL);
    if (result != 0) {
        fprintf(stderr, "Error on thread create, return value = %d\n", result);
        _exit(-1);
    }
    printf("Thread created, thid = %lu\n", thid);
    int c = 0;
    mythid = pthread_self();
    c = a;
    c = c + 1;
    a = c;
    printf("Thread %lu, calculation result = %d\n", mythid, a);
    pthread_join(thid, (void **) NULL);
    return 0;
}
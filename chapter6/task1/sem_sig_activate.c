#include "../../apue.h"
#include <sys/ipc.h>
#include <sys/sem.h>

int main(int argc, char **argv) {

    int semid;
    const char pathname[] = "sem_sig_activate.c";
    key_t key;
    struct sembuf mybuf;

    if ((key = ftok(pathname, 0)) < 0) {
        err_sys("Can't generate key!");
    }

    if ((semid = semget(key, 1, 0666 | IPC_CREAT)) < 0) {
        err_sys("Can't get semid!");
    }

    printf("semid = %d. Run `ipcs -s` to debug.\n", semid);

    mybuf.sem_op  = -1;
    mybuf.sem_flg =  0;
    mybuf.sem_num =  0;

    if (semop(semid, &mybuf, 1) < 0) {
        err_sys("Can't wait for a condition!");
    }

    puts("Condition is present!");

    return 0;
}
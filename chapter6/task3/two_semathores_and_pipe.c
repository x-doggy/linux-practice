/* Программа, осуществляющая однонаправленную связь через pipe 
между процессом-родителем и процессом-ребенком */ 
#include "../../apue.h"
#include <sys/sem.h>

int sem_create(key_t key) {
    return semget(key, 1, 0666 | IPC_CREAT);
}

int sem_dec(int id) {
    struct sembuf mybuf;
    mybuf.sem_op = -1;
    mybuf.sem_flg = 0;
    mybuf.sem_num = 0;
    return semop(id, &mybuf, 1);
}

int sem_inc(int id) {
    struct sembuf mybuf;
    mybuf.sem_op =  1;
    mybuf.sem_flg = 0;
    mybuf.sem_num = 0;
    return semop(id, &mybuf, 1);
}

int sem_del(int id) {
    return semctl(id, 0, IPC_RMID ); 
}


int main(int argc, char **argv) {

    int fd[2], result;
    size_t size;
    char resstring[14]; 
    int semid;

    if ((semid = sem_create(IPC_PRIVATE)) < 0) {
        err_sys("Can\'t create semaphore!");
    }
    
    if (pipe(fd) < 0) {
        err_sys("Can\'t create pipe!");
    } 

    result = fork();

    if (result < 0) { 
        err_sys("Can\'t fork child!");
    }
    else if (result > 0) {

        size = write(fd[1], "Hello, child!", 14);
        if (size != 14) {
            err_sys("Can\'t write all strings!"); 
        } 
        
        sem_dec(semid);
        
        size = read(fd[0], resstring, 14);
        if (size < 0) {
            err_sys("Can\'t read string!"); 
        }

        close(fd[0]);
        close(fd[1]);

        puts(resstring);
        puts("Parent returns");
        sem_del(semid);

    } else {

        size = read(fd[0], resstring, 14);
        if (size < 0) {
            err_sys("Can\'t read string!"); 
        }

        size = write(fd[1], "Hello, parent!", 14);
        if (size != 14) {
            err_sys("Can\'t write all string!");
        }
        
        sem_inc(semid);
        
        puts(resstring);
        puts("Child returns");

        close(fd[0]);
        close(fd[1]);
    }

    return 0; 
}

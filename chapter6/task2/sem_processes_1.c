#include "../../apue.h"
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <errno.h>

int main(int argc, char **argv) {

    int semid;
    char pathnamee[] = "a";
    key_t kkey;
    struct sembuf mybuf[2];

    if ((kkey = ftok(pathnamee, 0)) < 0) {
        err_sys("Can't generate key!");
    }

    if ((semid = semget(kkey, 1, 0666 | IPC_CREAT)) < 0) {
        err_sys("Can't get semid!");
    }

    mybuf[0].sem_op  = 0;
    mybuf[0].sem_flg = 0;
    mybuf[0].sem_num = 0;
    mybuf[1].sem_op  = 1;
    mybuf[1].sem_flg = 0;
    mybuf[1].sem_num = 0;

#ifdef _DEBUG
    puts("DEBUG");
    if ((semop(semid, &mybuf[0], 2)) < 0) {
        err_sys("Can't do operaton!");
    }
#endif

    int flag = 1;
    int *sm;
    int d;
    const char pathname[] = "b";
    key_t key;

    if ((key = ftok(pathname, 0)) < 0) {
        err_sys("Can't generate key!");
    }

    if ((d = shmget(key, 4 * sizeof(int), 0666 | IPC_CREAT | IPC_EXCL)) < 0) {
        if (errno != EEXIST) {
            err_sys("Can't create shared memory!");
        } else {
            if ((d = shmget(key, 4 * sizeof(int), 0)) < 0) {
                err_sys("Can't find shared memory!");
            }
            flag = 0;
        }
    }

    if ((sm = (int *) shmat(d, NULL, 0)) == (int *) (-1)) {
        err_sys("Can't attach shared memory!");
    }

    if (flag == 1) {
        sm[0] = 1;
        sm[1] = 0;
        sm[2] = 0;
        sm[3] = 1;
    } else {
        sm[0]++;
        sm[3]++;
    }

    printf("Run counter [%d]: %d\n", 1, sm[0]);
    printf("Run counter [%d]: %d\n", 2, sm[1]);
    printf("Run counter [%d]: %d\n", 3, sm[2]);
    printf("Sum counter: %d\n", sm[3]);

    if (shmdt(sm) < 0) {
        err_sys("Can't detach shared memory!");
    }

    mybuf[0].sem_op = -1;

    if (semop(semid, &mybuf[0], 1) < 0) {
        err_sys("Can't do operation!");
    }

    return 0;
}
#include "../../apue.h"
#include <dirent.h>

void d(const char *s) {

    struct stat buf;
    DIR *A;
    char s2[256] = {0};

    if ((A = opendir(s)) == NULL) {
        err_sys("opendir error!");
    }
    
    struct dirent *B = readdir(A);
    while (B != NULL) {
        if (strcmp(B->d_name, ".") && strcmp(B->d_name, "..")) {
            memset(s2, '\0', 256);
            strcpy(s2, s);
            strcat(s2, B->d_name);

            stat(s2, &buf);
            if (S_ISDIR(buf.st_mode)) {
                strcat(s2,"/");
                d(s2);
            } else {
               unlink(s2);
            }
        }
        B = readdir(A);
    }
    rmdir(s);
}

int main(int argc, char **argv) {

    char s[256] = {0};
    strcpy(s, "11/");
    d(s);
    return 0;
}

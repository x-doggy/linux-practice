#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <time.h>
#include <pwd.h>
#include <sys/param.h>

char* permissions(char *file) {
    struct stat st;
    char *modeval = malloc(sizeof(char) * 10 + 1);
    errno = 0;
    if (stat(file, &st) == 0) {
        mode_t perm = st.st_mode;

        if ((perm & S_ISUID) || (perm & S_ISGID)) {
            modeval[0] = 's';
        }
        else if (S_ISDIR(perm)) {
            modeval[0] = 'd';
        }
        else {
            modeval[0] = '-';
        }

        modeval[1] = (char) ((perm & S_IRUSR) ? 'r' : '-');
        modeval[2] = (char) ((perm & S_IWUSR) ? 'w' : '-');
        modeval[3] = (char) ((perm & S_IXUSR) ? 'x' : '-');
        modeval[4] = (char) ((perm & S_IRGRP) ? 'r' : '-');
        modeval[5] = (char) ((perm & S_IWGRP) ? 'w' : '-');
        modeval[6] = (char) ((perm & S_IXGRP) ? 'x' : '-');
        modeval[7] = (char) ((perm & S_IROTH) ? 'r' : '-');
        modeval[8] = (char) ((perm & S_IWOTH) ? 'w' : '-');
        modeval[9] = (char) ((perm & S_IXOTH) ? 'x' : '-');
        modeval[10] = '\0';
        return modeval;
    }

    return strerror(errno);
}

int list_dir(const char *dirname) {

    struct dirent *current_directory;
    struct stat my_stat;
    struct passwd *pwd;

    DIR *directory = opendir(dirname);

    if (directory == NULL) {
        printf("list_dir : %s : %s \n", dirname, strerror(errno));
        return 0;
    }

    char cwd[PATH_MAX];
    if (getcwd(cwd, sizeof(cwd)) != NULL) {
        printf("Current working dir: %s\n\n", cwd);
    } else {
        perror("getcwd() error");
        return -1;
    }

    while ((current_directory = readdir(directory))) {
        char buf[1024];
        errno = 0;
        nlink_t nlinks = 0;
        snprintf(buf, sizeof buf, "%s/%s", dirname, current_directory->d_name);
        if ( (stat(buf, &my_stat) ) == 0 ) {
            pwd = getpwuid(my_stat.st_uid);
            nlinks = my_stat.st_nlink;
        } else {
            printf("%s: %s\n", buf, strerror(errno));
            exit(-1);
        }

        char *perms = permissions(buf);

        // Last Modified
        struct tm *file_stat_gmtime = gmtime(&my_stat.st_mtime);
        char timebuf[64];
        strftime(timebuf, 64, "%b %d %H:%M", file_stat_gmtime);

        if (pwd != 0) {
            printf("%s  %d %s \t %ld \t %s \t %s\n", perms, (int) nlinks, pwd->pw_name, (long) my_stat.st_size, timebuf, current_directory->d_name);
        } else {
            printf("%s  %d %d \t %ld \t %s \t %s\n", perms, (int) nlinks, my_stat.st_uid, (long) my_stat.st_size, timebuf, current_directory->d_name);
        }

        free(perms);
    }

    closedir(directory);
    return 0;
}

int main(int argc, char *argv[]) {

    if (argc == 1) {
        return list_dir(".");
    } else {
        int ret = 0;
        for (int i = 1; i < argc; i += 1) {
            if (list_dir(argv[i]) != 0) {
                ret = 1;
            }
        }
        return ret;
    }
}
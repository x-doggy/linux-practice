#include "../../apue.h"
#include <sys/mman.h>

int main(int argc, char **argv) {

    int fd;
    size_t const STRSIZE = 50;
    size_t size;

    struct Base {
      char key[STRSIZE];
      char surname[STRSIZE];
      char name[STRSIZE];
      char patronymic[STRSIZE];
      int year;
    } *db, *db1;

    fd = open("Basedata", O_RDWR | O_CREAT | O_TRUNC, 0666);
    if (fd == -1) {
        err_sys("Error opening file!");
    }

    size = 5 * sizeof(struct Base);
    ftruncate(fd, size);
    
    db = (struct Base *) mmap(NULL, size, PROT_WRITE | PROT_READ, MAP_SHARED, fd, 0);
    close(fd);
    
    if (db == MAP_FAILED) {
        err_sys("Error file mapping!");
    }
    
    char per[STRSIZE];
    db1 = db;
    
    for (int i = 1; i <= 5; i++) {
        printf("№ %d\n", i);

        printf("%s", "Enter key: ");
        scanf("%s", &per[0]);
        for (int j = 0; j < STRSIZE; j++) {
            db1->key[j] = per[j];
        }

        printf("%s", "Enter surname: ");
        scanf("%s", &per[0]);
        for (int j = 0; j < STRSIZE; j++) {
            db1->surname[j] = per[j];
        }
       
        printf("%s", "Enter name: ");
        scanf("%s", &per[0]);
        for (int j = 0; j < STRSIZE; j++) {
            db1->name[j] = per[j];
        }
        
        printf("%s", "Enter fathername: ");
        scanf("%s", &per[0]);
        for (int j = 0; j < STRSIZE; j++) {
            db1->patronymic[j] = per[j];
        }
        
        printf("%s", "Enter birth: ");
        scanf("%s", &per[0]);
        db1->year = atoi(per);
        db1++;
    }

    munmap((void *) db, size);
    return 0;
}

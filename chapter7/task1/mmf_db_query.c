#include "../../apue.h"
#include <sys/mman.h>

void menu() {
    puts("1-Вывод записей с заданным значением ключа");
    puts("2-Вывод всех записей с одинаковым значение полей (кроме ключа)");
    puts("3-Вывод всех записей со значением года рождения в заданном диапазоне");
    puts("4-Выход");
}

int main(int argc, char **argv) {

    int fd;
    size_t size;
    size_t const STRSIZE = 50;

    struct Base {
        char key[STRSIZE];
        char surname[STRSIZE];
        char name[STRSIZE];
        char patronymic[STRSIZE];
        int year;
    } *db, *db1, *db2;
    
    fd = open("Basedata", O_RDWR, 0666);
    if (fd == -1) {
        err_sys("Error opening file!");
    }

    size = 5 * sizeof(struct Base);
    db = (struct Base *) mmap(NULL, size, PROT_WRITE | PROT_READ, MAP_SHARED, fd, 0);
    close(fd);
    if (db == MAP_FAILED) {
        err_sys("Error file mapping!");
    }

    char f = ' ';
    
    while (f != 27) {
        db1 = db;
        menu();
        
        f = getchar();
        switch (f) {
        case '1': {
            char per[STRSIZE * 10];
            char per1[STRSIZE];
            printf("Enter key: ");
            scanf("%s", &per1[0]);
            scanf("%s", &per[0]);
            
            int flag = 0;
            for (int i = 1; i <= 5; i++) {
                if (!strcmp(per,db1->key)) {
                    flag = 1;
                    printf("Key: %s\n", db1->key);
                    printf("Surname: %s\n", db1->surname);
                    printf("Name: %s\n", db1->name);
                    printf("Fathername: %s\n", db1->patronymic);
                    printf("Birth: %d\n\n", db1->year);
                }
                db1++;
            }

            if (!flag) {
                puts("Not found!");
            }
            break;
        }
        
        case '2': {
            int mas[6] = {0};
            int flag = 0, flag2 = 0;

            for (int i = 1; i <= 4; i++) {
                if (mas[i] == 1) continue;
                db2 = db1;
                db2++;

                for (int j = i + 1; j <= 5; j++) {
                    if (mas[j] == 1) continue;
    
                    if (
                        !strcmp(db1->surname, db2->surname) &&
                        !strcmp(db1->name, db2->name) &&
                        !strcmp(db1->patronymic, db2->patronymic) &&
                        db1->year == db2->year
                    ) {
                        printf("Key: %s\n", db1->key);
                        printf("Surname: %s\n", db1->surname);
                        printf("Name: %s\n", db1->name);
                        printf("Fathername: %s\n", db1->patronymic);
                        printf("Birth: %d\n\n", db1->year);
                        
                        if (!flag) {
                            mas[i] = 1;
                            flag = 1;
                            flag2 = 1;
                        }
                        mas[j] = 1;
                    }
    
                    if (j != 5) db2++;
                }
                db1++;
            }
            
            if (!flag2) {
                puts("Not found!");
            }
            break;
        }
        
        case '3': {
            char per2[STRSIZE * 10];
            char per[STRSIZE];
            
            printf("%s", "Enter low: ");
            scanf("%s", &per2[0]);
            scanf("%s", &per[0]);
            int a = atoi(per);
            
            printf("%s", "Enter high: ");
            scanf("%s", &per[0]);
            int b = atoi(per);
            
            int flag = 0;
            
            for (int i = 1; i <= 5; i++) {
                if (db1->year >= a && db1->year <= b) {
                    flag = 1;
                    printf("Key: %s\n", db1->key);
                    printf("Surname: %s\n", db1->surname);
                    printf("Name: %s\n", db1->name);
                    printf("Fathername: %s\n", db1->patronymic);
                    printf("Birth: %d\n\n", db1->year);
                }
                db1++;
            }

            if (!flag) {
                puts("Not found!");
            }
            break;
        }
        
        case '4': break;
        }
    }

    munmap((void *) db, size);
    return 0;
}

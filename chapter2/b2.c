#include "../apue.h"
#include <sys/wait.h>
#include <math.h>

#define cotan(f) ( 1 / tan(f) )

int main(int argc, char **argv) {

    switch( fork() ) {
    /* Ошибка */
    case -1:
        err_sys("fork");

    /* Процесс-потомок */
    case 0:
        for (double i=0.; i<=1.; i+=atof(argv[1])) {
            if (i >= 0.5f)
                exit(0);
            printf("cotan %f = %f\t\tPID = %d\tPPID = %d\n", i, cotan(i), getpid(), getppid());
        }
        exit(0);

    /* Процесс-родитель */
    default:
        for (double i=0.; i<=1.; i+=atof(argv[1])) {
            if (i >= 0.5f) exit(1);
            printf("tan %f = %f\tPID = %d\tPPID = %d\n", i, tan(i), getpid(), getppid());
        }
        wait(0);
    }
    return 0;
}
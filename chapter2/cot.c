#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define cotan(f) ( 1 / tan(f) )

int main(int argc, char **argv) {
    if (argc == 1) return 0;
    const double arg = atof(argv[1]);
    for (double i=0.; i<=1; i+=arg) {
        if (i == 0.5f) exit(1);
        printf("ARG = %f\tRES = %f\n", i, cotan(i));
    }
    return 0;
}
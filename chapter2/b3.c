#include "../apue.h"
#include <sys/wait.h>
#include <math.h>

int main(int argc, char **argv, char **envp) {

    const double arg = atof(argv[1]);

    switch( fork() ) {
    /* Ошибка */
    case -1:
        err_sys("fork");

    /* Процесс-потомок */
    case 0:
        printf("PID = %d\t\tPPID = %d\n", getpid(), getppid());
        execle("./cot", "./cot", argv[1], 0, envp);
        exit(0);

    /* Процесс-родитель */
    default:
        printf("PID = %d\t\tPPID = %d\n", getpid(), getppid());
        execl("./tan", "./tan", argv[1], 0, envp);
        wait(0);
    }
    return 0;
}